#!/bin/bash
FROM jenkins/jenkins

USER root

RUN apt-get update && apt-get install -y apt-transport-https \
       ca-certificates curl gnupg2 \
       software-properties-common \
       python \
       python3-pip

FROM ubuntu:latest 

RUN \
# Update
apt-get update -y && \
# Install Unzip
apt-get install unzip -y && \
# need wget
apt-get install wget -y && \
# Install Terraform
#Download terraform for linux
RUN wget https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip

# Unzip
RUN unzip terraform_0.11.11_linux_amd64.zip

# Move to local bin
RUN mv terraform /usr/local/bin/
# Check that it's installed
RUN terraform --version 
       
USER jenkins

RUN jenkins-plugin-cli --plugins "gitlab-api:1.0.6 gitlab-oauth:1.10 gitlab-plugin:1.5.20"

USER root